package com.progressoft.pdp.demopdp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPdpApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoPdpApplication.class, args);
    }

}

package com.progressoft.pdp.demopdp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("api/v1/requests")
public class MainController {

    public static final Random RANDOM = new Random();
    public static List<Integer> requests = new ArrayList<>();

    @GetMapping
    public List<Integer> getRequests() {
        return requests;
    }

    @PostMapping
    public List<Integer> newRequest() {
        requests.add(RANDOM.nextInt());
        return requests;
    }

}


package com.progressoft.pdp.demopdp;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class MainControllerTest {

    private MainController mainController = new MainController();

    @Test
    void whenCreateNewRequest_thenShouldUpdateTheRequestsList() {
        List<Integer> integers = mainController.newRequest();
        Assertions.assertThat(integers).hasSizeGreaterThan(0);

        List<Integer> getRequestIntegers = mainController.getRequests();
        Assertions.assertThat(getRequestIntegers).containsAll(integers);
    }


}